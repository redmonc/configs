;; Emacs config
;; Charlie Redmon
;; 20220106

;; Attribution: This config file was heavily inspired by
;; Peter Prevos' tutorial:
;;   lucidmanager.org/productivity/more-productive-with-emacs/

;; define init file
(setq custom-file (expand-file-name "custom.el" user-emacs-directory))
(when (file-exists-p custom-file)
  (load custom-file))

;; configure backups
(setq backup-directory-alist '(("" . "/home/cr/.emacs.d/backups/")))
(setq backup-by-copying t)

;; define and initialize package repositories
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(package-initialize)

;; basic UI configs
(setq inhibit-startup-message t)   ; turn off startup screen
(tool-bar-mode -1)                 ; no toolbar
(scroll-bar-mode -1)               ; no scrollbar
(defalias 'yes-or-no-p 'y-or-n-p)  ; change yes/no Q to y/n
(global-display-line-numbers-mode) ; line numbers

;; theme
(load-theme 'leuven t)

;; configure encoding
(set-language-environment "UTF-8")

;; text completion with Company
(global-company-mode)
(setq company-idle-delay 0)
(setq company-minimum-prefix-length 3)
(setq company-selection-wrap-around t)

;; configure Org Mode
;;; basic configs
(setq org-startup-indented t
      org-pretty-entities t
      org-hide-emphasis-markers t
      org-startup-with-inline-images t
      org-image-actual-width '(300))
;;; show hidden characters when hovering over
(require 'org-appear)

;;; nicer bullets
(require 'org-superstar)
  :config
(setq org-superstar-special-todo-items t)
(add-hook 'org-mode-hook (lambda ()
                           (org-superstar-mode 1)))

;; Configure ESS
(require 'ess-r-mode)
(setq lsp-diagnostics-provider :none)
(define-key ess-r-mode-map "_" #'ess-insert-assign)
(define-key inferior-ess-r-mode-map "_" #'ess-insert-assign)


;; Configure LaTeX
(eval-after-load "tex"
  '(progn
     (setq TeX-view-program-list '(("Qpdfview" "qpdfview-qt5 --page-index=%(outpage) %o")))
     ;(add-to-list 'TeX-command-list '("View" "evince %g" TeX-run-command nil t :help "Run evince on your document"))
     (setq TeX-view-program-selection '((output-pdf "Qpdfview")))))


